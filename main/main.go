package main

import (
	"log"
	"net/http"
	"vtalk-live-web-service/pkg/handlers"
	"vtalk-live-web-service/pkg/handlers/websockethandler"
	"vtalk-live-web-service/pkg/utils"

	ghandler "github.com/gorilla/handlers"
)

func main() {

	//Read the config
	conf, err := utils.ReadConfig("config/config.json")
	if err != nil {
		log.Fatal("config Error: ", err.Error())
	}

	// Context
	ctxt := &utils.Context{Config: conf}

	// start webSocket Hub
	hub := websockethandler.NewHub()
	go hub.Run()

	// start room store
	roomStore := websockethandler.NewRoomStore()
	go roomStore.StartRooms()

	webSocketHandler := websockethandler.GetWebSocketContents(ctxt, hub, roomStore)

	availableHandlers := []handlers.HandlerInterface{webSocketHandler}

	router := handlers.AddHandlers(availableHandlers)

	header := ghandler.AllowedHeaders([]string{
		"X-Requested-With",
		"Content-Type",
		"Authorization",
		"AuthorizationGrant",
	})
	methods := ghandler.AllowedMethods([]string{
		"GET",
		"POST",
		"PUT",
		"DELETE"})
	origins := ghandler.AllowedOrigins([]string{"*"})

	err = http.ListenAndServe(":4447", ghandler.CORS(header, methods, origins)(router))
	if err != nil {
		log.Fatal("ListenAndServe error =" + err.Error())
	}
}
