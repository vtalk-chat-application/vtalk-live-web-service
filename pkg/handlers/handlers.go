package handlers

import "github.com/gorilla/mux"

type HandlerInterface interface {
	AddHandler(router *mux.Router)
}

func AddHandlers(handlers []HandlerInterface) *mux.Router {

	router := mux.NewRouter()

	for _, handler := range handlers {
		handler.AddHandler(router)
	}

	return router
}
