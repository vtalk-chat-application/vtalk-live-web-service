package websockethandler

import (
	"encoding/json"
	"fmt"
	"log"
	"sync"
	"time"
	"vtalk-live-web-service/pkg/model"
)

const (
	RoomPermissionHost   = "host"
	RoomPermissionCoHost = "cohost"
	RoomPermissionUser   = "user"

	RoomTypeCall = "call"
	RoomTypeMeet = "meet"

	RoomStatusCreated = "created"
	RoomStatusLive    = "live"
	RoomStatusStopped = "stop"
	RoomStatusDeleted = "deleted"
)

type RoomStore struct {
	Mutex sync.RWMutex

	// Store all rooms data.
	Rooms map[string]*RoomHub

	// CreateRoom
	createRoom chan *RoomHub

	// remove user from the room by passing user id(ws)
	removeFromAllRoom chan string

	broadcastToRoom chan model.WsMessageBody
}

type RoomHub struct {
	// unique room id
	RoomId string

	// name of the room (if any)
	RoomName string

	// room type- call, meeting
	RoomType string

	// video, audio (audio and video calls)
	VideoCall bool

	// id to get user websocket client connection
	OwnerWsUserId string

	// indicate current status of the room- not started - live - stopped
	Status string

	// store all joined participants data
	Participants map[string]*Participant

	// RoomUsers all user name and permission level who has access without permission
	RoomUsers []RoomUser
}

type RoomUser struct {
	// user id for websocket connection
	WsUserId string

	// user permission level for the room - host, cohost, user and spectator(always audio & video are mute)
	PermissionLevel string

	// time used to check before auto join. update the time when the ws connection closes or on each ping.
	LastActiveTime time.Time
}

type Participant struct {

	// user id for websocket connection
	WsUserId string

	// User ws connection
	Client *Client

	// time used to check before auto join. update the time when the ws connection closes or on each ping.
	LastActiveTime time.Time
}

func NewRoomStore() *RoomStore {
	return &RoomStore{
		Rooms:             make(map[string]*RoomHub),
		createRoom:        make(chan *RoomHub),
		removeFromAllRoom: make(chan string),
		broadcastToRoom:   make(chan model.WsMessageBody),
	}
}

func (rs *RoomStore) StartRooms() {
	for {
		select {
		case room := <-rs.createRoom:
			rs.Rooms[room.RoomId] = room

		// case roomPar := <-rs.JoinRoom:

		// delete participant from all room who has the client id
		case parClientId := <-rs.removeFromAllRoom:
			log.Println("*****---------removeFromAllRoom ")
			for _, room := range rs.Rooms {
				delete(room.Participants, parClientId)
			}

		case wsMessageBody := <-rs.broadcastToRoom:
			log.Println("<<-- broadcast chan")
			if room, err := rs.GetBroadcastToRoomFromWSMessageBody(wsMessageBody); err == nil {
				broadcastMessageBody, err := json.Marshal(map[string]interface{}{
					"sender": wsMessageBody.Sender,
					"event":  wsMessageBody.Event,
					"data":   wsMessageBody.Data,
					"code":   200,
				})
				if err != nil {
					log.Println("broadcastToRoom json Marshal error ", err)

				} else {

					log.Println("broadcastToRoom loop on ")

					for _, p := range room.Participants {
						log.Println("sender: ", wsMessageBody.Sender, " broadcastMessageBody participant ", p.Client.ClientID)
						if wsMessageBody.Sender != p.Client.ClientID {
							log.Println("broadcastMessageBody event", wsMessageBody.Event)
							p.Client.send <- broadcastMessageBody
						}
					}
					log.Println("broadcastToRoom loop done ")
				}
			} else {
				log.Println("GetBroadcastToRoomFromWSMessageBody failed")
			}
		}
	}
}

func (rs *RoomStore) GetRoomParticipants(roomId string) map[string]*Participant {
	rs.Mutex.Lock()
	defer rs.Mutex.Unlock()

	return rs.Rooms[roomId].Participants
}

func (rs *RoomStore) GetBroadcastToRoomFromWSMessageBody(wsMessageBody model.WsMessageBody) (*RoomHub, error) {
	data, ok := wsMessageBody.Data.(map[string]interface{})
	if !ok {
		log.Println("broadcastToRoom failed to convert interface")
		return nil, fmt.Errorf("failed")
	}
	roomIdVal, ok := data["room_id"]
	if !ok {
		log.Println("broadcastToRoom failed to get room_id")
		return nil, fmt.Errorf("failed")

	}
	roomId, ok := roomIdVal.(string)
	if !ok {
		log.Println("broadcastToRoom failed to parse room_id")
		return nil, fmt.Errorf("failed")

	}

	room, ok := rs.Rooms[roomId]
	if !ok {
		log.Println("broadcastToRoom room not found")
		return nil, fmt.Errorf("failed")
	}
	return room, nil
}
