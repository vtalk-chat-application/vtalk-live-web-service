package websockethandler

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"time"
	"vtalk-live-web-service/pkg/model"
	"vtalk-live-web-service/pkg/servicecall"

	"github.com/gorilla/mux"
)

type CreateRoomRequestModel struct {
	RoomType  string   `json:"type"`
	RoomName  string   `json:"name"`
	VideoCall bool     `json:"video"`
	RoomUsers []string `json:"users"`
}

// create a new room for user as host
func (wc *WebSocketContents) CreateRoom(w http.ResponseWriter, r *http.Request) {

	var response servicecall.Response

	authorization := r.Header.Get("Authorization")
	if authorization == "" {
		response.WriteError(w, http.StatusUnauthorized, servicecall.ErrUnauthorized, "authorization required")
		return
	}
	authResp, userData := wc.ctxt.VerifyUserAuthentication(authorization)
	if authResp.HttpStatus != http.StatusOK {
		authResp.ForwardResponse(w)
		return
	}

	var crRequest CreateRoomRequestModel
	if err := json.NewDecoder(r.Body).Decode(&crRequest); err != nil {
		response.WriteError(w, http.StatusBadRequest, servicecall.ErrBadRequest, "invalid request body")
		return
	}

	createdRoomUser := RoomUser{
		WsUserId:        userData.UserID,
		PermissionLevel: RoomPermissionHost,
	}

	// add created user to room users
	var allRoomUsers = []RoomUser{createdRoomUser}

	switch crRequest.RoomType {
	case RoomTypeCall:
		if len(crRequest.RoomUsers) != 1 {
			response.WriteError(w, http.StatusBadRequest, servicecall.ErrBadRequest, "only one user is allowed")
			return
		} else if crRequest.RoomUsers[0] == userData.UserID {
			response.WriteError(w, http.StatusBadRequest, servicecall.ErrBadRequest, "a different user id required to make call room")
			return
		}

		// TODO - verify user id !
		allRoomUsers = append(allRoomUsers, RoomUser{
			WsUserId:        crRequest.RoomUsers[0],
			PermissionLevel: RoomPermissionUser,
		})
	case RoomTypeMeet:
		// TODO - verify RoomUsers id !
		for _, userId := range crRequest.RoomUsers {
			allRoomUsers = append(allRoomUsers, RoomUser{
				WsUserId:        userId,
				PermissionLevel: RoomPermissionUser,
			})
		}
	default:
		response.WriteError(w, http.StatusBadRequest, servicecall.ErrBadRequest, "invalid room type")
		return
	}

	log.Printf(" >>>>>> users %+v", allRoomUsers)

	roomData := RoomHub{
		RoomId:        wc.ctxt.GenerateUniqueId("room-"),
		RoomName:      crRequest.RoomName,
		RoomType:      crRequest.RoomType,
		VideoCall:     crRequest.VideoCall,
		OwnerWsUserId: userData.UserID,
		Status:        RoomStatusCreated,
		RoomUsers:     allRoomUsers,
		Participants:  map[string]*Participant{},
	}

	wc.roomStore.createRoom <- &roomData

	response.WriteResult(w, http.StatusCreated, map[string]interface{}{"room_id": roomData.RoomId})

	if crRequest.RoomType == RoomTypeCall {
		wc.SendCallInvitation(roomData.RoomId, userData.Name, crRequest.RoomUsers[0])
	}

}

// Send invitation to call room user
func (wc *WebSocketContents) SendCallInvitation(roomId, senderName, recvId string) bool {
	broadcastMessageBody, err := json.Marshal(map[string]interface{}{
		"sender": senderName,
		"event":  "call_invite",
		"data": map[string]interface{}{
			"room_id": roomId,
			"from":    senderName,
		},
		"code": 200,
	})
	if err != nil {
		log.Println("call_invite failed")
		return false
	}

	// check if the receiver is connected to socket
	// if we got connection send call invitation to the connection
	// else we check at every user new ws connection tine for any pending calls
	if uClients, ok := wc.wsHub.userClients[recvId]; ok {
		for _, clientId := range uClients {

			clientSubscription, ok := wc.wsHub.clientSubscriptions[clientId]
			if !ok || clientSubscription.client.WsConn == nil {
				continue
			}

			// Send data to client websocket connection using send channel
			clientSubscription.client.send <- broadcastMessageBody
		}

	}
	return true
}

func (wc *WebSocketContents) GetRoomParticipants(w http.ResponseWriter, r *http.Request) {

	var response servicecall.Response

	vars := mux.Vars(r)
	roomId, ok := vars["roomid"]
	if !ok {
		response.WriteError(w, http.StatusUnauthorized, servicecall.ErrInvalidRequest, "invalid room id")
		return
	}
	authorization := r.Header.Get("Authorization")
	if authorization == "" {
		response.WriteError(w, http.StatusUnauthorized, servicecall.ErrUnauthorized, "authorization required")
		return
	}
	// authResp, userData := wc.ctxt.VerifyUserAuthentication(authorization)
	// if authResp.HttpStatus != http.StatusOK {
	// 	authResp.ForwardResponse(w)
	// 	return
	// }

	// upgrade this check later with permited and joied user
	room, ok := wc.roomStore.Rooms[roomId]
	if !ok {
		response.WriteError(w, http.StatusBadRequest, servicecall.ErrBadRequest, "room not found")
		return
	}
	// if room.OwnerWsUserId != userData.UserID {
	// 	response.WriteError(w, http.StatusUnauthorized, servicecall.ErrUnauthorized, "authorization required")
	// 	return
	// }

	response.WriteResult(w, http.StatusFound, room.Participants)
}

type joinRoomModel struct {
	RoomId string `json:"room_id"`
}

func JoinRoom(s *Subscription, wsMessageBody model.WsMessageBody) {

	log.Println("Join room Func in")

	byteData, err := json.Marshal(wsMessageBody.Data)
	if err != nil {
		log.Println("failed to marshal")
		return
	}

	var jRoom joinRoomModel
	err = json.Unmarshal(byteData, &jRoom)
	if err != nil {
		log.Println("failed to unmarshal")
		return
	}

	participant := Participant{
		WsUserId:       s.userWsId,
		Client:         s.client,
		LastActiveTime: time.Now(),
	}

	log.Println("<<-- JOIN ROOM ", jRoom.RoomId)

	if room, ok := s.wc.roomStore.Rooms[jRoom.RoomId]; ok {

		// validate user
		// if room type is call check if the user id is in users list and the current number of participant is less than 2
		if room.RoomType == RoomTypeCall {
			if len(room.Participants) == 2 {
				// TODO  - handle error no more participant allowed
				log.Println("err: join call - no more participant allowed")
				return
			}
			var valid bool = false
			for _, user := range room.RoomUsers {
				if user.WsUserId == s.userWsId {
					valid = true
					break
				}
			}
			if !valid {
				// TODO  - handle error user not listed
				log.Println("chek user ", s.userWsId)
				log.Println("err: join call - user not listed", room.RoomUsers)
				return
			}
			// check if any participant is already joined for the same user in this room - duplicate user not allowed
			for _, p := range room.Participants {
				if p.WsUserId == s.userWsId {
					// TODO  - handle error duplicate user not allowed
					log.Println("err: join call - duplicate user not allowed")
					return
				}
			}
		}

		room.Participants[participant.Client.ClientID] = &participant

		broadcastMessageBody, err := json.Marshal(map[string]interface{}{
			"sender": participant.WsUserId,
			"event":  WsEventJoinRoom,
			"data": map[string]interface{}{
				"room_id": jRoom.RoomId,
			},
			"code": 200,
		})

		if err != nil {
			log.Println("failed to marshal broadcastMessageBody")
			return
		}

		log.Println("send<- joined_resp ")
		participant.Client.send <- broadcastMessageBody

		var wsMessageBody model.WsMessageBody
		wsMessageBody.Sender = participant.Client.ClientID
		wsMessageBody.Event = "user_joined"
		wsMessageBody.Data = map[string]interface{}{
			"room_id": jRoom.RoomId,
		}
		log.Println("broad cast join")
		s.wc.roomStore.broadcastToRoom <- wsMessageBody
	}
}

var i = 0

func (wc *WebSocketContents) EnquireSocket(w http.ResponseWriter, r *http.Request) {

	var response servicecall.Response
	if i == 1 {
		i = 0
	} else {
		go func() {
			x := 0
			i = 1
			fmt.Println(" log hub data")
			for i == 1 {
				time.Sleep(1 * time.Second)
				fmt.Printf("ws_hub: %+v\n", wc.wsHub.clientSubscriptions)
				fmt.Println(x, "\n     -----------------     \n")
				fmt.Printf("Room Store: %+v\n", wc.roomStore.Rooms)
				fmt.Println(x, "\n     -----------------     \n")
				x++
			}
		}()
	}

	// fmt.Printf("ws_hub:  %+v", wc.wsHub.clientSubscriptions)
	// fmt.Printf("room_store: %+v\n", wc.roomStore.Rooms)

	response.WriteResult(w, http.StatusOK, "console")

}
