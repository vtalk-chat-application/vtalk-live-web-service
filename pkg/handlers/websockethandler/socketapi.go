package websockethandler

import (
	"net/http"

	"github.com/gorilla/mux"
)

func (wc *WebSocketContents) AddHandler(router *mux.Router) {

	//public api to connect userId
	router.HandleFunc("/ws/connect/user/room", wc.serveWebSocketUserConnection).Methods(http.MethodGet)

	wc.AddInternalHandlerV1(router.PathPrefix("/service/api/v1/ws").Subrouter())
	wc.AddSocketHandlerV1(router.PathPrefix("/api/v1").Subrouter())

}

// internal api
func (wc *WebSocketContents) AddInternalHandlerV1(wsRouter *mux.Router) {

	wsRouter.HandleFunc("/broadcast/chat", wc.ctxt.ValidateServiceToken(wc.serveWebSocketDistributeChat)).Methods(http.MethodPost)

}

func (wc *WebSocketContents) AddSocketHandlerV1(wsRouter *mux.Router) {

	wsRouter.HandleFunc("/create/meet/room", wc.CreateRoom).Methods(http.MethodPost)
	wsRouter.HandleFunc("/meet/room/{roomid}/participants", wc.GetRoomParticipants).Methods(http.MethodGet)

	wsRouter.HandleFunc("/enq/socket/info", wc.EnquireSocket).Methods(http.MethodGet)

}
