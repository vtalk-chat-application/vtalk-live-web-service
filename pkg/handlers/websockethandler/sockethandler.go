package websockethandler

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"vtalk-live-web-service/pkg/servicecall"
	"vtalk-live-web-service/pkg/utils"
)

type WebSocketContents struct {
	ctxt      *utils.Context
	wsHub     *WebsocketHub
	roomStore *RoomStore
}

func GetWebSocketContents(ctxt *utils.Context, hub *WebsocketHub, roomStore *RoomStore) *WebSocketContents {
	return &WebSocketContents{
		ctxt:      ctxt,
		wsHub:     hub,
		roomStore: roomStore,
	}
}

// serveWs handles websocket requests from the peer.
func (wc *WebSocketContents) serveWebSocketUserConnection(w http.ResponseWriter, r *http.Request) {

	query := r.URL.Query()
	auth, authOk := query["Auth"]
	if !authOk || auth[0] == "" {
		fmt.Print("ws:Err: Unauthorized: token Notfound")
		http.Error(w, "Err: Unauthorized", http.StatusUnauthorized)
		return
	}

	upgrader.CheckOrigin = func(r *http.Request) bool {
		// check origin
		return true
	}

	// create new websocket connection by upgrading the current http connection to websocket connection.
	wsConn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println(err.Error())
		return
	}

	newClient := &Client{

		// unique id for client
		ClientID: wc.ctxt.GenerateUniqueId("cl-"),

		// send chanel to send data to the connection
		send: make(chan []byte, 256),

		// websocket connection
		WsConn: wsConn,
	}

	// veryfy auth
	authResp, userData := wc.ctxt.VerifyUserAuthentication(auth[0])
	if authResp.HttpStatus != http.StatusOK {
		fmt.Print("ws:Err: Unauthorized: Invalid token")
		errResp, _ := json.Marshal(map[string]interface{}{"error": "Unauthorized", "code": 401})
		newClient.send <- errResp
		close(newClient.send)
		return
	}

	// create new ws subscription for the user
	s := &Subscription{
		client:   newClient,
		userWsId: userData.UserID, // use user id for the identification of user websocket connection for data transfer.
		wc:       wc,
	}

	// register user websocket connection to hub
	wc.wsHub.register <- s

	go s.writePump()
	go s.readPump()
}

func (wc *WebSocketContents) serveWebSocketDistributeChat(w http.ResponseWriter, r *http.Request) {
	// verify service token

	// log.Println("debug: message received")

	var response servicecall.Response
	// ctxt := r.Context()
	requestData, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Println("serveWebSocketDistributeChat: ReadAll failed", err.Error())
		response.WriteError(w, http.StatusBadRequest, servicecall.ErrBadRequest, "Input missing")
		return
	}

	m := WebSocketMessage{data: requestData}
	m.HandleEventMessage(wc)
}
