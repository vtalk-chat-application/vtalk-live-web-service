package websockethandler

import (
	"log"
	"time"

	"github.com/gorilla/websocket"
)

const (
	// Time allowed to write a message to the peer.
	writeWait = 10 * time.Second

	// Time allowed to read the next pong message from the peer.
	pongWait = 60 * time.Second

	// Send pings to peer with this period. Must be less than pongWait.
	pingPeriod = (pongWait * 9) / 10

	// Maximum message size allowed from peer.
	maxMessageSize = 100000
)

// upgrader will up grade the http connection to a websocket connection.
var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

// Client is an middleman between the websocket connection and the hub.
type Client struct {

	// A unique id for each connection
	ClientID string

	// The websocket connection.
	WsConn *websocket.Conn

	// Buffered channel of outbound messages.
	send chan []byte
}

// subscription will connect the client with the userId, hub details and configs.
type Subscription struct {
	client   *Client
	userWsId string
	wc       *WebSocketContents
}

// readPump pumps messages from the websocket connection to the hub.
func (s *Subscription) readPump() {
	c := s.client
	defer func() {
		s.wc.wsHub.unregister <- s
		c.WsConn.Close()
	}()
	c.WsConn.SetReadLimit(maxMessageSize)
	c.WsConn.SetReadDeadline(time.Now().Add(pongWait))
	c.WsConn.SetPongHandler(func(string) error { c.WsConn.SetReadDeadline(time.Now().Add(pongWait)); return nil })
	for {
		_, msg, err := c.WsConn.ReadMessage()
		if err != nil {
			if websocket.IsUnexpectedCloseError(err, websocket.CloseGoingAway) {
				log.Printf("error: %v", err)
			}
			break
		}
		m := WebSocketMessage{data: msg, userWsId: s.userWsId}
		m.HandleOpenEventMessage(s)
	}
}

// write writes a message with the given message type and payload.
func (c *Client) write(messageType int, payload []byte) error {
	c.WsConn.SetWriteDeadline(time.Now().Add(writeWait))
	return c.WsConn.WriteMessage(messageType, payload)
}

// writePump pumps messages from the hub to the websocket connection.
func (s *Subscription) writePump() {
	c := s.client
	ticker := time.NewTicker(pingPeriod)
	defer func() {
		ticker.Stop()
		c.WsConn.Close()
	}()
	for {
		select {
		case message, ok := <-c.send:
			if !ok {
				c.write(websocket.CloseMessage, []byte{})
				return
			}

			if err := c.write(websocket.TextMessage, message); err != nil {
				log.Println("write fail err: ", err.Error())
				return
			}
		case <-ticker.C:
			if err := c.write(websocket.PingMessage, []byte{}); err != nil {
				return
			}
		}
	}
}
