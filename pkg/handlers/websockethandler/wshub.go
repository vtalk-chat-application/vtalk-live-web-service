package websockethandler

import (
	"encoding/json"
	"log"
	"vtalk-live-web-service/pkg/model"
)

type WebsocketHub struct {
	// Registered clients.
	clientSubscriptions map[string]*Subscription

	// store all user clients => map[wsUserId]= clientId
	userClients map[string][]string

	// Register requests from the clients.
	register chan *Subscription

	// Unregister requests from clients.
	unregister chan *Subscription

	// Broad cast message to all user in the chat audience list.
	broadcaster chan *model.WsMessageBody
}

func NewHub() *WebsocketHub {
	return &WebsocketHub{
		register:            make(chan *Subscription),
		unregister:          make(chan *Subscription),
		broadcaster:         make(chan *model.WsMessageBody),
		clientSubscriptions: make(map[string]*Subscription),
		userClients:         map[string][]string{},
	}
}

func (wh *WebsocketHub) Run() {
	for {
		select {
		case sub := <-wh.register:
			wh.clientSubscriptions[sub.client.ClientID] = sub

			if _, ok := wh.userClients[sub.userWsId]; ok {
				wh.userClients[sub.userWsId] = append(wh.userClients[sub.userWsId], sub.client.ClientID)
			} else {
				wh.userClients[sub.userWsId] = []string{sub.client.ClientID}
			}

		case sub := <-wh.unregister:

			// get the client details using client Id
			if _, ok := wh.clientSubscriptions[sub.client.ClientID]; ok {

				// Remove the clientId from userClient array.
				if userClients, ok := wh.userClients[sub.userWsId]; ok {
					for i, v := range userClients {
						if v == sub.client.ClientID {
							wh.userClients[sub.userWsId] = removeFromStringSlice(wh.userClients[sub.userWsId], i)
						}
					}
				}
				// remove client connection from rooms
				for _, r := range sub.wc.roomStore.Rooms {
					delete(r.Participants, sub.client.ClientID)
				}

				// delete the ws subscription.
				delete(wh.clientSubscriptions, sub.client.ClientID)
				close(sub.client.send)
			}

		case wsMessageBody := <-wh.broadcaster:

			// This is the ui model to receive data
			broadcastMessageBody, err := json.Marshal(map[string]interface{}{
				"sender": wsMessageBody.Sender,
				"event":  wsMessageBody.Event,
				"data":   wsMessageBody.Data,
				"code":   200,
			})
			if err == nil {
				for _, receiver := range wsMessageBody.Receivers {
					if receiver != wsMessageBody.Sender {

						if uClients, ok := wh.userClients[receiver]; ok {
							for _, clientId := range uClients {
								clientSubscription, ok := wh.clientSubscriptions[clientId]
								if !ok || clientSubscription.client.WsConn == nil {
									continue
								}

								// Send data to client websocket connection using send channel
								clientSubscription.client.send <- broadcastMessageBody
							}
						}

					}
				}
			} else {
				log.Println("invalid broadcast message body ", wsMessageBody)
			}
		}
	}
}

func removeFromStringSlice(s []string, i int) []string {
	s[i] = s[len(s)-1]
	return s[:len(s)-1]
}
