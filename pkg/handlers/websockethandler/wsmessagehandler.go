package websockethandler

import (
	"encoding/json"
	"log"
	"vtalk-live-web-service/pkg/model"
)

type WebSocketMessage struct {
	data     []byte
	userWsId string
}

const (
	BroadcastChat = "broad_chat"

	BroadcastChatToSelfClients = "self_client_chat"

	WsEventJoinRoom = "join_room"
)

// This is an Open func to send receive data directly from user and send to the receivers.
// Mostly going to use for one time message transmissions. ie; which is not always stored in db.
func (m *WebSocketMessage) HandleOpenEventMessage(s *Subscription) {
	// This is the ui model to receive data
	var wsMessageBody model.WsMessageBody

	if err := json.Unmarshal(m.data, &wsMessageBody); err != nil {
		return
	}

	switch wsMessageBody.Event {
	case BroadcastChatToSelfClients:
		selfClientsBroadCast(wsMessageBody, s)
	case WsEventJoinRoom:
		JoinRoom(s, wsMessageBody)

		// room open messages
	case "peer_offer", "peer_answer", "ice_candidate":
		wsMessageBody.Sender = s.client.ClientID
		s.wc.roomStore.broadcastToRoom <- wsMessageBody

	default:
		log.Println("invalid ws open event")
		return
	}
}

// This will handle all important message transmission.
func (m *WebSocketMessage) HandleEventMessage(wc *WebSocketContents) {
	var wsMessageBody model.WsMessageBody

	if err := json.Unmarshal(m.data, &wsMessageBody); err != nil {
		return
	}

	switch wsMessageBody.Event {
	case BroadcastChat:

		wc.wsHub.broadcaster <- &wsMessageBody
		// broadcast message
		// broadcaste(wsMessageBody, wc)

	default:

		return
	}
}

// broad cast message to all other clients of the same user
func selfClientsBroadCast(wsMessageBody model.WsMessageBody, s *Subscription) {

	broadcastMessageBody, err := json.Marshal(map[string]interface{}{
		"event": wsMessageBody.Event,
		"data":  wsMessageBody.Data,
		"code":  200,
	})
	if err != nil {
		log.Println("selfClientsBroadCast: Json Marshal error= ", err.Error())
		return
	}
	if uClients, ok := s.wc.wsHub.userClients[s.userWsId]; ok {
		for _, clientId := range uClients {
			if clientId == s.client.ClientID {
				continue
			}

			clientSubscription, ok := s.wc.wsHub.clientSubscriptions[clientId]
			if !ok || clientSubscription.client.WsConn == nil {
				continue
			}

			// Send data to client websocket connection using send channel
			clientSubscription.client.send <- broadcastMessageBody
		}
	}
}
