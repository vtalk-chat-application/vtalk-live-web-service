package model

import "github.com/golang-jwt/jwt"

type UserCredentialModel struct {
	Name            string `json:"name,omitempty" bson:"name"`
	UserID          string `json:"user_id,omitempty" bson:"user_id"`
	EmailID         string `json:"email_id,omitempty" bson:"email_id"`
	Verified        int64  `json:"verified,omitempty" bson:"verified"` // 1 or 0
	PermissionLevel string `json:"permission_level,omitempty" bson:"permission_level"`
}

type ChatModel struct {
	ChatID      string   `json:"chat_id,omitempty" bson:"chat_id"`
	Name        string   `json:"name,omitempty" bson:"name"`
	Description string   `json:"description,omitempty" bson:"description"`
	Type        string   `json:"type,omitempty" bson:"type"`
	Audience    []string `json:"audience,omitempty" bson:"audience"`
	CreatedBy   string   `json:"created_by,omitempty" bson:"created_by"`
	CreatedDate int64    `json:"created_date,omitempty" bson:"created_date"`
	UpdatedDate int64    `json:"updated_date,omitempty" bson:"updated_date"`
}
type MessageModel struct {
	MessageID   string `json:"message_id,omitempty"`
	ChatID      string `json:"chat_id,omitempty"`
	MessageData string `json:"message_data,omitempty"`
	MessageType string `json:"message_type,omitempty"`
	CreatedBy   string `json:"created_by,omitempty"`
	CreatedDate int64  `json:"created_date,omitempty"`
	UpdatedDate int64  `json:"updated_date,omitempty"`
}
type BroadcastMessageData struct {
	ChatData ChatModel    `json:"chat,omitempty"`
	Message  MessageModel `json:"message,omitempty"`
}


type WsMessageBody struct {
	Sender    string      `json:"sender" validate:"required"`
	Receivers []string    `json:"receivers" validate:"required"`
	Event     string      `json:"event" validate:"required"`
	Data      interface{} `json:"data" validate:"required"`
}

type Claims struct {
	UserDetails UserCredentialModel
	jwt.StandardClaims
}
