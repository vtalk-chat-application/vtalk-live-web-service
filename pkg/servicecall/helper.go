package servicecall

import "net/http"

// Generic Error codes
const (
	ErrInvalidRequest = "ERR_INVR_400"
	ErrBadRequest     = "ERR_BDRQ_400"
	ErrUnauthorized   = "ERR_UATH_401"
	ErrNotFound       = "ERR_NTFD_404"
	ErrDuplicate      = "ERR_DPCT_409"

	ErrInternalServerError = "ERR_INSE_500"
)

// Some common error, pre-built
func Error(code string) Response {
	var resp Response
	switch code {
	case ErrBadRequest:
		resp.ResponseError.Code = ErrBadRequest
		resp.ResponseError.Message = "bad request"
		resp.HttpStatus = http.StatusBadRequest

	case ErrUnauthorized:
		resp.ResponseError.Code = ErrUnauthorized
		resp.ResponseError.Message = "unauthorized request"
		resp.HttpStatus = http.StatusUnauthorized
	default:
		resp.ResponseError.Code = ErrInternalServerError
		resp.ResponseError.Message = "internal server error"
		resp.HttpStatus = http.StatusInternalServerError
		return resp
	}
	return resp
}
