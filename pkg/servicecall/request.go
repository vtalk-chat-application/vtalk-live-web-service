package servicecall

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"

	"github.com/golang-jwt/jwt"
)

func (sc *Request) CallServiceAPI() (Response, error) {
	var response Response

	client := &http.Client{}

	var req *http.Request
	var err error

	completeUrl := sc.BaseURL + "/service" + sc.Path

	if sc.PayloadByte != nil {
		req, err = http.NewRequest(sc.Method, completeUrl, bytes.NewBuffer(sc.PayloadByte))
	} else {
		req, err = http.NewRequest(sc.Method, completeUrl, sc.PayloadReadCloser)
	}
	if err != nil {
		return response, err
	}

	claims := &Claims{
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: time.Now().Add(time.Minute * 2).Unix()},
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	jwtTokenString, err := token.SignedString([]byte(sc.AccessTokenSecrect))
	if err != nil {
		return response, err
	}

	for k, v := range sc.Header {
		req.Header.Add(k, v[0])
	}

	req.Header.Add("AccessToken", jwtTokenString)

	req.Close = true

	req.Header.Add("Accept", "application/json")
	req.Header.Add("Content-Type", "application/json; charset=UTF-8")
	resp, err := client.Do(req)
	if err != nil {
		return response, err
	}
	defer resp.Body.Close()

	bodyBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return response, err
	}

	response.HttpStatus = int64(resp.StatusCode)

	if err = json.Unmarshal(bodyBytes, &response); err != nil {
		if resp.StatusCode != http.StatusOK {
			fmt.Printf("log: response format error: actual data= %v resp= %v", string(bodyBytes), resp)
			response.ResponseError.Code = "ERR_SC_FORMAT_4000"
			response.HttpStatus = http.StatusBadRequest
		} else {
			response.ResponseError.Code = "ERR_G_500"
		}
		response.ResponseError.Message = string(bodyBytes)

		return response, nil
	}

	return response, nil
}
