package servicecall

import (
	"encoding/json"
	"log"
	"net/http"
)

func (r *Response) WriteResult(w http.ResponseWriter, httpStatusCode int64, result interface{}) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")

	if httpStatusCode != 0 {
		r.HttpStatus = httpStatusCode
	} else {
		r.HttpStatus = 200
	}

	switch res := result.(type) {
	case string:
		r.Message = res
	default:
		r.Result = res
	}

	if r.Result == nil {
		r.Result = make(map[string]interface{})
	}

	w.WriteHeader(int(r.HttpStatus))
	json.NewEncoder(w).Encode(r)
}

func (r *Response) WriteError(w http.ResponseWriter, httpStatusCode int64, errCode, errMessage string) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")

	if httpStatusCode != 0 {
		r.HttpStatus = httpStatusCode
	} else {
		r.HttpStatus = 500
	}

	r.Result = make(map[string]interface{})

	r.ResponseError.Code = errCode
	r.ResponseError.Message = errMessage

	if errCode == "" {
		r.ResponseError.Code = ErrInternalServerError
	}

	w.WriteHeader(int(r.HttpStatus))
	json.NewEncoder(w).Encode(r)
}
func (r *Response) ForwardResponse(w http.ResponseWriter) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")

	if r.HttpStatus == 0 {
		r.HttpStatus = http.StatusInternalServerError
	}

	if r.Result == nil {
		r.Result = make(map[string]interface{})
	}

	if r.Result == nil && r.ResponseError.Code == "" && r.Message == "" {
		r.ResponseError.Code = ErrInternalServerError
		r.HttpStatus = http.StatusInternalServerError
		log.Println("ForwardResponse: response handler error!")
	}

	w.WriteHeader(int(r.HttpStatus))
	json.NewEncoder(w).Encode(r)
}
