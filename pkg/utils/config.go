package utils

import (
	"encoding/base64"
	"log"
	"os"

	"github.com/spf13/viper"
)

type Config struct {
	APIVersion      APIVersion
	ServiceTokenKey string
	ServiceCall     ServiceCall
	InternalApiURL  InternalApiURL
}
type APIVersion struct {
	APIVersionKey string
	APIVersionV1  string
}

type ServiceCall struct {
	UserService    ServiceData
	ContentService ServiceData
}
type ServiceData struct {
	AccessToken string
	BaseURL     string
}
type InternalApiURL struct {
	UserServiceAPI    UserServiceAPI
	ContentServiceAPI ContentServiceAPI
}
type UserServiceAPI struct {
	VerifyAuth string
}

type ContentServiceAPI struct {
	GetChatMetadata string
}

// ReadConfig reads the config json
// This will read the json file from the given file path.
// Basicaly it reads the json file using a package called viper.
//  vipor will read it and assign it to a values in struct object Config.
// The read call is basicaly from the initial stage of the main file.
// Then the config will be added to a context struct as pointer refernce, so that the config values can be readed from anywere from the code.
func ReadConfig(configfile string) (*Config, error) {

	// Set the vipor reading file path
	viper.SetConfigFile(configfile)

	// Searches for config file in given paths and read it
	if err := viper.ReadInConfig(); err != nil {
		return nil, err
	}

	// Merge the current config data with the updateConfig file.
	updateConfig := "./config/updateconfig/config.json" // Path to add update config file

	if _, err := os.Stat(updateConfig); !os.IsNotExist(err) {
		viper.SetConfigFile(updateConfig)
		err := viper.MergeInConfig()
		if err != nil {
			return nil, err
		}
	}

	conf := &Config{}

	conf.APIVersion.APIVersionKey = viper.GetString("api_version.api_version_key")
	conf.APIVersion.APIVersionV1 = viper.GetString("api_version.api_version_v1")

	stk, err := base64.StdEncoding.DecodeString(viper.GetString("service_token_key"))
	if err != nil {
		log.Fatal("config 2 Error: ", err.Error())
	}
	conf.ServiceTokenKey = string(stk)

	// apis

	ust, err := base64.StdEncoding.DecodeString(viper.GetString("service_call.user_service.access_token"))
	if err != nil {
		log.Fatal("config 3 Error: ", err.Error())
	}
	conf.ServiceCall.UserService.AccessToken = string(ust)
	conf.ServiceCall.UserService.BaseURL = viper.GetString("service_call.user_service.baseurl")

	cst, err := base64.StdEncoding.DecodeString(viper.GetString("service_call.content_service.access_token"))
	if err != nil {
		log.Fatal("config 4 Error: ", err.Error())
	}
	conf.ServiceCall.ContentService.AccessToken = string(cst)
	conf.ServiceCall.ContentService.BaseURL = viper.GetString("service_call.content_service.baseurl")

	// api urls
	conf.InternalApiURL.UserServiceAPI.VerifyAuth = viper.GetString("internal_apiurl.user_service_api.verifyauth")

	conf.InternalApiURL.ContentServiceAPI.GetChatMetadata = viper.GetString("internal_apiurl.content_service_api.getchatmetadata")

	return conf, nil
}
