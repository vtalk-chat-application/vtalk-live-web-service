package utils

//Context has all the important handlers needed for the application and will not change
// Thes context will be passed to all thethe handelrs as a context
type Context struct {
	Config *Config
}
