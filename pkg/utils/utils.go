package utils

import (
	"encoding/json"
	"log"
	"net/http"
	"strings"
	"vtalk-live-web-service/pkg/model"
	"vtalk-live-web-service/pkg/servicecall"

	"github.com/google/uuid"
	"github.com/rs/xid"
)

//generate unique ID
func (c *Context) GenerateUniqueId(idPrefix string) string {
	uuid, err := uuid.NewRandom()
	if err != nil {
		return idPrefix + xid.New().String()
	}
	return idPrefix + uuid.String()
}

func (c *Context) VerifyUserAuthentication(authorization string) (servicecall.Response, model.UserCredentialModel) {
	var serviceRequest servicecall.Request
	serviceRequest.Header = make(http.Header)
	serviceRequest.Header.Add("Authorization", authorization)

	var userData model.UserCredentialModel
	serviceRequest.Path = c.Config.InternalApiURL.UserServiceAPI.VerifyAuth
	serviceRequest.Method = http.MethodPost
	serviceRequest.BaseURL = c.Config.ServiceCall.UserService.BaseURL
	serviceRequest.AccessTokenSecrect = c.Config.ServiceCall.UserService.AccessToken
	resp, err := serviceRequest.CallServiceAPI()
	if err != nil {
		return servicecall.Error(servicecall.ErrInternalServerError), userData
	}
	if resp.HttpStatus != http.StatusOK {
		return resp, userData
	}
	dataByte, err := json.Marshal(resp.Result)
	if err != nil {
		return servicecall.Error(servicecall.ErrInternalServerError), userData
	}
	if err = json.Unmarshal(dataByte, &userData); err != nil {
		return servicecall.Error(servicecall.ErrInternalServerError), userData
	}
	return resp, userData
}

func (c *Context) VerifyUserChatId(userId, chatId string) (servicecall.Response, model.ChatModel) {
	var serviceRequest servicecall.Request
	serviceRequest.Header = make(http.Header)

	var chatData model.ChatModel

	reqHeader := make(http.Header)
	reqHeader.Add("token_user_id", userId)

	apiurl := strings.Replace(c.Config.InternalApiURL.ContentServiceAPI.GetChatMetadata, "{chatid}", chatId, 1)
	serviceRequest.Path = apiurl
	serviceRequest.Method = http.MethodGet
	serviceRequest.Header = reqHeader
	serviceRequest.BaseURL = c.Config.ServiceCall.ContentService.BaseURL
	serviceRequest.AccessTokenSecrect = c.Config.ServiceCall.ContentService.AccessToken
	resp, err := serviceRequest.CallServiceAPI()
	if err != nil {
		return servicecall.Error(servicecall.ErrInternalServerError), chatData
	}
	if resp.HttpStatus != http.StatusOK {
		return resp, chatData
	}
	dataByte, err := json.Marshal(resp.Result)
	if err != nil {
		return servicecall.Error(servicecall.ErrInternalServerError), chatData
	}
	if err = json.Unmarshal(dataByte, &chatData); err != nil {
		return servicecall.Error(servicecall.ErrInternalServerError), chatData
	}
	return resp, chatData
}

func (c *Context) CallService(r *http.Request, s ServiceData) (servicecall.Response, error) {
	var serviceRequest servicecall.Request
	serviceRequest.Path = r.URL.Path
	serviceRequest.Method = r.Method
	serviceRequest.Header = r.Header
	serviceRequest.PayloadReadCloser = r.Body
	serviceRequest.BaseURL = s.BaseURL
	serviceRequest.AccessTokenSecrect = s.AccessToken
	resp, err := serviceRequest.CallServiceAPI()
	if err != nil {
		log.Printf("CallServiceAPI URI: %s err %s", serviceRequest.BaseURL+serviceRequest.Path, err.Error())
	}
	return resp, err
}
