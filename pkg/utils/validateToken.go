package utils

import (
	"encoding/json"
	"net/http"
	"vtalk-live-web-service/pkg/model"

	"github.com/golang-jwt/jwt"
)

// temp use user service token for all case
func (c *Context) ValidateServiceToken(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		claims := &model.Claims{}
		tokenString := r.Header.Get("accessToken")

		tokenScrect := c.Config.ServiceTokenKey
		tkn, err := jwt.ParseWithClaims(tokenString, claims,
			func(t *jwt.Token) (interface{}, error) {
				return []byte(tokenScrect), nil
			})
		if err != nil {
			w.Header().Set("Content-Type", "application/json; charset=UTF-8")
			w.WriteHeader(int(500))
			json.NewEncoder(w).Encode(err.Error())
			return
		}
		if !tkn.Valid {
			w.Header().Set("Content-Type", "application/json; charset=UTF-8")
			w.WriteHeader(int(400))
			json.NewEncoder(w).Encode(err.Error())
		}

		// call API handler
		next(w, r)
	}
}
